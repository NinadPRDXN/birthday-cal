/* Author: 
    Ninad Parkar
*/
import Sorter from './sorter.js'

let
form = document.querySelector('.user_input_form'),
api = [
    {
        name: "Tyrion Lannister",
        birthday: "12/02/1978"
    },
    {
        name: "Cersei Lannister",
        birthday: "11/30/1975"
    },
    {
        name: "Daenerys Targaryen",
        birthday: "11/24/1991"
    },
    {
        name: "Arya Stark",
        birthday: "11/25/1996"
    },
    {
        name: "Jon Snow",
        birthday: "12/03/1989"
    },
    {
        name: "Sansa Stark",
        birthday: "15/08/1992"
    },
    {
        name: "Jorah Mormont",
        birthday: "12/02/1975"
    }
],
text_area = document.querySelector('textarea');

//For Displaying Api in TextBox
text_area.value = `[\n`;
api.forEach( item => {
    text_area.value +=
     `    {
        name: ${item.name},
        birthday: ${item.birthday}
    },`;
});
text_area.value += `\n]`;

//Invoke after submit is clicked
form.onsubmit = e => {
    let userInput = document.querySelector('.year input').value,
    newSorter = new Sorter(userInput, api);
    add(newSorter.monday, ".mon_output");
    add(newSorter.tuesday, ".tue_output");
    add(newSorter.wednesday, ".wed_output");
    add(newSorter.thursday, ".thu_output");
    add(newSorter.friday, ".fri_output");
    add(newSorter.saturday, ".sat_output");
    add(newSorter.sunday, ".sun_output");
    return false;
}

//To Choose random color
let getRandomColor = () => {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

//To Add content in the boxes
let add = (array, classname) => {
    let
    target = document.querySelector(classname),
    size = array.length;
    target.innerHTML = ""
    if ( size == 0 ) {
        target.innerHTML =
        `<div style="
        width: 100%;
        height: 100%;
        display: flex;
        align-items: center;
        background-color: #dee4e6;
        font-size: 2rem;
        justify-content: center;
        "><span>Empty</span></div>`;
    }
    else if ( size == 1 ) {
        let 
        personData = array[0].name.split(' '),
        data = [],
        newColor = getRandomColor();

        personData.forEach( item => {
            data.push(item.substring(0, 1).toUpperCase()); 
        });

        target.innerHTML =
        `<div style="
        width: 100%;
        height: 100%;
        display: flex;
        align-items: center;
        background-color: ${newColor};
        color: #ffffff;
        font-size: 2rem;
        justify-content: center;
        "><span>${data[0] + data[1]}</span></div>`;
    }
    else if ( size == 2 ) {
        array.forEach( x => {
            let
            personName = x.name.split(' '),
            data = [],
            newColor = getRandomColor();
            
            personName.forEach( item => {
                data.push(item.substring(0, 1).toUpperCase()); 
            });

            target.innerHTML +=
            `<div style="
            width: 50%;
            height: 50%;
            display: flex;
            align-items: center;
            background-color: ${newColor};
            color: #ffffff;
            font-size: 2rem;
            justify-content: center;
            "><span>${data[0] + data[1]}</span></div>`;
        });
    }
    else {
        let
        division = (Math.floor(size / 3)) + 1,
        boxWidth = 100 / division;
        array.forEach( x => {
            let
            personName = x.name.split(' '),
            data = [],
            newColor = getRandomColor();
            
            personName.forEach( item => {
                data.push(item.substring(0, 1).toUpperCase()); 
            });

            target.innerHTML +=
            `<div style="
            width: ${boxWidth}%;
            display: flex;
            align-items: center;
            background-color: ${newColor};
            color: #ffffff;
            font-size: 2rem;
            justify-content: center;
            "><span>${data[0] + data[1]}</span></div>`;
        });
    }
}
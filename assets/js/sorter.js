//For Sorting each according to their Output
export default class Sorter {
    let
    days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
    months = ['January','February','March','April','May','June','July','August','September','October','November','December']
    monday = []
    tuesday = []
    wednesday = []
    thursday = []
    friday = []
    saturday = []
    sunday = [];

    constructor(userInput, api) {
        api.forEach( item => {
            let parts = item.birthday.split('/');
            let now = new Date(userInput, parts[1] - 1, parts[0]);
            let day = this.days[ now.getDay() ];
            if (day == "Monday") this.pushMonday(item);
            else if (day == "Tuesday") this.pushTuesday(item);
            else if (day == "Wednesday") this.pushWednesday(item);
            else if (day == "Thursday") this.pushThursday(item);
            else if (day == "Friday") this.pushFriday(item);
            else if (day == "Saturday") this.pushSaturday(item);
            else if (day == "Sunday") this.pushSunday(item);
        });
    }

    pushMonday(data) {
        this.monday.push(data);
    }

    pushTuesday(data) {
        this.tuesday.push(data);
    }

    pushWednesday(data) {
        this.wednesday.push(data);
    }

    pushThursday(data) {
        this.thursday.push(data);
    }

    pushFriday(data) {
        this.friday.push(data);
    }

    pushSaturday(data) {
        this.saturday.push(data);
    }

    pushSunday(data) {
        this.sunday.push(data);
    }
}